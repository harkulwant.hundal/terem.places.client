import * as actions from '../places';

jest.mock('../places'); 

describe('actions > places tests', () => {
  let fnGetPlaces;

  beforeEach(() => {
    fnGetPlaces = jest.spyOn(actions, 'getPlaces');
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('load places', async () => {
    expect.assertions(3);

    const result = await actions.getPlaces();

    expect(fnGetPlaces).toHaveBeenCalledTimes(1);
    expect(result.data).toHaveLength(2);
    expect(result.data[0].title).toEqual('I am the greatest');
  
  });
  
});