/* actions to manage places */
import axios from 'axios';

const baseUrl = 'http://demo3136867.mockable.io';

const fetch = async (endpoint) => {
  try {
    const response = await axios.get(endpoint);
    return response.data;
  } catch(ex) {
    console.error('fetch failed: ', ex);
  }
  return [];
}

const getEndpointFragment = (type) => {
  switch(type){
    case 'popular-around-you':
      return 'carousel';
    
    case 'featured':
      return 'featured';

    default:
      return 'carousel';
  }
}

/* GET places */
export const getPlaces = async (type) => {
  try {
    return await fetch(`${baseUrl}/${getEndpointFragment(type)}`);
  } catch(ex) {
    console.error('getPopularAroundYouPlaces failed: ', ex);
  }
};