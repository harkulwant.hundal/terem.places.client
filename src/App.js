import './App.css';
import PlacesPage from './components/places';

function App() {
  return (
    <div className="App">
      <PlacesPage />
    </div>
  );
}

export default App;
