import renderer from 'react-test-renderer';
import Listing from '../listing';
import Tile from '../tile';

jest.mock('../../../actions/places');

describe('components > places > listing tests', () => {

  const props = {
    title: 'Popular around you',
    type: 'carousel',
    search: 'greatest',
    tileWidth: '300',
    tileHeight: '200'
  };

  afterEach(() => {
    // cleanup on exiting
    jest.resetAllMocks();
  });
  
  it('<Listing /> should render correctly', async () => {

    let basic;

    // render component
    await renderer.act(async() => {
      basic = renderer.create(<Listing 
        title={props.title}
        type={props.type}
        tileWidth={props.tileWidth} 
        tileHeight={props.tileHeight} 
      />);

    });

    expect(basic.toJSON()).toMatchSnapshot();
    
    const instance = basic.root;
    expect(instance.findAllByProps({type: props.type})).toHaveLength(1);
    expect(instance.findAllByType(Tile)).toHaveLength(2);

  });


  it('<Listing /> should render carsoul', async () => {

    let carousel;

    // render component
    await renderer.act(async() => {

      carousel = renderer.create(<Listing 
        title={props.title}
        type={props.type}
        search={props.search} 
        tileWidth={props.tileWidth} 
        tileHeight={props.tileHeight}
        carousel
      />);

    });

    expect(carousel.toJSON()).toMatchSnapshot();

    const instance = carousel.root;
    expect(instance.findAllByProps({type: props.type})).toHaveLength(1);
    expect(instance.findAllByType(Tile)).toHaveLength(2);

  });

});
