import renderer from 'react-test-renderer';
import { act } from 'react-dom/test-utils';
import { render, screen } from '@testing-library/react';
import PlacesPage from '../index';

jest.mock('../../../actions/places');

describe('components > places > index tests', () => {

  afterEach(() => {
    // cleanup on exiting
    jest.resetAllMocks();
  });
  
  it('<PlacesPage /> should render correctly', async () => {

    let placesPage;

    // render component
    await renderer.act(async() => {
      placesPage = renderer.create(<PlacesPage/>);
    });

    expect(placesPage.toJSON()).toMatchSnapshot();
    
  });


  it('<PlacesPage /> should render search box', async () => {
    expect.assertions(2);

    // render component
    await act(async() => {
      render(<PlacesPage/>);
    });

    const searchBoxes = screen.getAllByPlaceholderText(/search for.../i);
    expect(searchBoxes).toHaveLength(1);
    
    const searchBox = searchBoxes[0];
    expect(searchBox).toHaveAttribute('type', 'search');
  });

});
