import renderer from 'react-test-renderer';
import { render, screen } from '@testing-library/react';
import Tile from '../tile';

describe('components > places > tile tests', () => {

  const props = {
    tileWidth: '300',
    tileHeight: '200'
  };

  const place = {
    img: 'https://i.pinimg.com/originals/ee/f2/2f/eef22f4add36ba425285c824188abf3e.jpg',
    title: 'I am the greatest',
    location: 'My Attitude'
  }

  const index = 1;

  it('<Tile /> should render', () => {
  
    const tree = renderer
    .create(<Tile 
      key={index} 
      img={place.img} 
      title={place.title} 
      location={place.location}
      hide={place.hide}
      width={props.tileWidth}
      height={props.tileHeight}
      >
      </Tile>)
    .toJSON();
      
    expect(tree).toMatchSnapshot();

  });

  it('<Tile /> should render <img /> element', () => {

    render(<Tile 
      key={index} 
      img={place.img} 
      title={place.title} 
      location={place.location}
      hide={place.hide}
      width={props.tileWidth}
      height={props.tileHeight}
      >
      </Tile>);
    const imageElements = screen.getAllByAltText(place.title);

    expect(imageElements).toHaveLength(1);
    
    const imageElement = imageElements[0];
    expect(imageElement).toHaveAttribute('width', props.tileWidth);
    expect(imageElement).toHaveAttribute('height', props.tileHeight);
    expect(imageElement).toHaveAttribute('loading', 'lazy');
    expect(imageElement).toHaveAttribute('src', place.img);
    
  });

  it('<Tile /> should render <h5 /> element', () => {

    render(<Tile 
      key={index} 
      img={place.img} 
      title={place.title} 
      location={place.location}
      hide={place.hide}
      width={props.tileWidth}
      height={props.tileHeight}
      >
      </Tile>);
    const headerElement = screen.getByText(place.title);

    expect(headerElement).toBeInTheDocument();
    
  });

  it('<Tile /> should render <span /> element', () => {

    render(<Tile 
      key={index} 
      img={place.img} 
      title={place.title} 
      location={place.location}
      hide={place.hide}
      width={props.tileWidth}
      height={props.tileHeight}
      >
      </Tile>);
    const spanElement = screen.getByText(place.location);

    expect(spanElement).toBeInTheDocument();
    
  });

  it('<Tile /> should be hidden when class hide found', () => {

    const {container } = render(<Tile 
      key={index} 
      img={place.img} 
      title={place.title} 
      location={place.location}
      hide={true}
      width={props.tileWidth}
      height={props.tileHeight}
      >
      </Tile>);

    expect(container.firstChild).toHaveClass('tile');
    expect(container.firstChild).toHaveClass('hide');
  });

});
