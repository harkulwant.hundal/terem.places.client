/* component to display the places */

import { getPlaces } from '../../actions/places';
import React, { useState, useEffect, createRef } from 'react';
import Tile from './tile';
import $ from 'jquery';
import './styles/listing.css';

const Listing = (props) => {

  /* using ref is one way to achieve the scroll on html element */
  const srcollRef = createRef();

  const [places, setPlaces] = useState([]);
  const [hide, setHidden] = useState(false);
  const [scrollActive, setScrollActive] = useState(false);

  // Effect callbacks are synchronous to prevent race conditions. Put the async function inside:
  useEffect(() => {
    const fetchPlaces = async () => {
      const result = await getPlaces(props.type);
      setPlaces(result.data);
    }

    fetchPlaces();
  },[props.type]);

  useEffect(() => {
    const searches = places.map((place) => {
      place.hide = !(place.title.toLowerCase().includes(props.search) || place.location.toLowerCase().includes(props.search));
      return place;
    });
    setPlaces(searches);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.search]);


  useEffect(() => {
    const hidden = places.filter(place => {
      return place.hide === true;
    });

    setHidden(hidden.length === places.length);
  }, [places]);

  const onClickToScroll = e => {
    e.preventDefault();
    if(scrollActive)
      return;

    setScrollActive(true);

    const box = $(srcollRef.current);
    let x;
    const $clicker = $(e.target);

    if($clicker.hasClass('arrow-right')){
      x = ((box.width() / 2)) + box.scrollLeft();
          box.animate({
            scrollLeft: x,
          })
    }else{
      x = ((box.width() / 2)) - box.scrollLeft();
          box.animate({
            scrollLeft: -x,
          })
    }
    setScrollActive(false);

  };

  return(
    <div className={`listing ${(hide? 'hide' : '')}`}>
      <div className='listing-wrapper'>
        <h4>{props.title}</h4>
        <div className={`${(props.carousel ? 'listing-carousel' : '')}`} ref={srcollRef} >
          <button className={`listing-carousel arrow-left arrow ${(props.carousel ? '' : 'hide')}`} onClick={onClickToScroll} disabled={scrollActive}>&lsaquo;</button>
          <button className={`listing-carousel arrow-right arrow ${(props.carousel ? '' : 'hide')}`} onClick={onClickToScroll} disabled={scrollActive}>&rsaquo;</button>
          {
            places.map((place, index) => {
              return <Tile 
                key={index} 
                img={place.img} 
                title={place.title} 
                location={place.location}
                hide={place.hide}
                width={props.tileWidth}
                height={props.tileHeight}
                >
                </Tile>
            })
          }
        </div>
      </div>
    </div>
  )
}

export default Listing;