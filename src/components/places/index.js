/* component to display the places */

import React, { useState } from 'react';
import Listing from './listing';
import './styles/index.css';

const PlacesPage = () => {

  const [search, setSearch] = useState('');

  const onChangeSearch = e => {
    setSearch(e.target.value);
  };

  return (
    <div className="places">
      <input type='search' placeholder='Search for...' value={search} onChange={onChangeSearch} />

      {/* Listing component can be isolated further using HOC to pass in the listing component & datastore
        * Currently, looking up places inside the component via actions
       */}
      <Listing 
        title='Popular around you' 
        type='places-around-you' 
        search={search}
        carousel 
        tileWidth='350' 
        tileHeight='200' 
      />

      <Listing 
        title='Featured' 
        type='featured' 
        tileWidth='225' 
        tileHeight='200' 
      />
    </div>
  )

}

export default PlacesPage;